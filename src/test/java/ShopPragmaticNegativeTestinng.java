import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.nio.file.Paths;

import static java.util.concurrent.TimeUnit.SECONDS;

public class ShopPragmaticNegativeTestinng {

    private static final By LOC_MYACCOUNTBUTTON = By.xpath("//span[2]");
    private static final By LOC_REGISTER = By.linkText("Register");
    private static final By LOC_LOGIN = By.linkText("Login");
    private static final By LOC_ID = By.id("input-firstname");
    private static final By LOC_LASTID = By.id("input-lastname");
    private static final By LOC_EMAIL = By.id("input-email");
    private static final By LOC_TELEPHONE = By.id("input-telephone");
    private static final By LOC_PASSWORD = By.id("input-password");
    private static final By LOC_INPUT_CONFIRM = By.id("input-confirm");
    private static final By LOC_NEWSLETTER = By.name("newsletter");
    private static final By LOC_POLICYAGREEMENTBUTTON = By.name("agree");
    private static final By LOC_CONTINUEBUTTON = By.xpath("//input[@value='Continue']");
    private static final By LOC_WARNINGMASSAGE = By.xpath("//div[@class='alert alert-danger alert-dismissible']");
    private static final By LOC_WMFIRSTNAME=By.xpath("//fieldset[@id='account']/div[2]/div/div");


    public WebDriver driver;
    @BeforeMethod
    public void setUp() {
        String chromePath = Paths.get("chromedriver.exe").toAbsolutePath().toString();
        System.setProperty("webdriver.chrome.driver", chromePath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, SECONDS);
    }

    @Test
    public void negativeTesting() {
        driver.get("http://shop.pragmatic.bg/");
        WebElement myAccountButton = driver.findElement(LOC_MYACCOUNTBUTTON);
        myAccountButton.click();
        driver.findElement(LOC_REGISTER).click();
        driver.findElement(LOC_CONTINUEBUTTON).click();
        WebElement warningMessage = driver.findElement(LOC_WARNINGMASSAGE);
        Assert.assertEquals(warningMessage.getText(), "Warning: You must agree to the Privacy Policy!");

        driver.findElement(LOC_WARNINGMASSAGE);
        // НЯМАМ ВРЕМЕ ВЕЧЕ



    }
    @AfterMethod
    public void tearDown() {
        // driver.quit();
    }
}
