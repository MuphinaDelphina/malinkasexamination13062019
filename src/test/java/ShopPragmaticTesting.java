import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.nio.file.Paths;

import static java.util.concurrent.TimeUnit.SECONDS;

public class ShopPragmaticTesting {
    private static final By LOC_MYACCOUNTBUTTON = By.xpath("//span[2]");
    private static final By LOC_REGISTER = By.linkText("Register");
    private static final By LOC_LOGIN = By.linkText("Login");
    private static final By LOC_ID = By.id("input-firstname");
    private static final By LOC_LASTID = By.id("input-lastname");
    private static final By LOC_EMAIL = By.id("input-email");
    private static final By LOC_TELEPHONE = By.id("input-telephone");
    private static final By LOC_PASSWORD = By.id("input-password");
    private static final By LOC_INPUT_CONFIRM = By.id("input-confirm");
    private static final By LOC_NEWSLETTER = By.name("newsletter");
    private static final By LOC_POLICYAGREEMENTBUTTON = By.name("agree");
    private static final By LOC_CONTINUEBUTTON = By.xpath("//input[@value='Continue']");


    public WebDriver driver;

    @BeforeMethod
    public void setUp() {
        String chromePath = Paths.get("chromedriver.exe").toAbsolutePath().toString();
        System.setProperty("webdriver.chrome.driver", chromePath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, SECONDS);
    }

    @Test
    public void shopPragmaticTesting() {
        driver.get("http://shop.pragmatic.bg/");
        WebElement myAccountButton = driver.findElement(LOC_MYACCOUNTBUTTON);
        myAccountButton.click();
        Assert.assertEquals(driver.getTitle(), "Pragmatic Test Store");

        driver.findElement(LOC_REGISTER).click();
        Assert.assertEquals(driver.getTitle(), "Register Account");

        driver.findElement(LOC_ID).sendKeys("Malinka");
        driver.findElement(LOC_LASTID).sendKeys("Bogdanova");
        driver.findElement(LOC_EMAIL).sendKeys("marinkabogdanova@gmail.com");
        driver.findElement(LOC_TELEPHONE).sendKeys("0886409865");
        driver.findElement(LOC_PASSWORD).sendKeys("malinka78");
        driver.findElement(LOC_INPUT_CONFIRM).sendKeys("malinka78");
        WebElement newsletterSubscription = driver.findElement(LOC_NEWSLETTER);

        WebElement policyChecker = driver.findElement(LOC_POLICYAGREEMENTBUTTON);

        if (!policyChecker.isSelected())
            policyChecker.click();

        Assert.assertTrue(policyChecker.isSelected());

        driver.findElement(LOC_CONTINUEBUTTON).click();

        Assert.assertEquals(driver.getTitle(), "Your Account Has Been Created!");


    }

    @AfterMethod
    public void tearDown() {
        // driver.quit();
    }
}
